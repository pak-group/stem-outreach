This repository contains Python-based workshops for various K-12 activities.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/pak-group%2Fstem-outreach/main)

