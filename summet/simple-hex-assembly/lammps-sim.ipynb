{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running, Visualizing, and Analyzing Molecular Dynamics (MD) Simulations ##\n",
    "In this notebook, you will use Python tools to play around with Molecular Dynamics (MD) simulations. In this example, we will prepare a system of interacting hexamers that may spontaneously self-assemble into hexameric sheets. The hexamers look like the following:\n",
    "\n",
    "<div>\n",
    "<img src=\"toy_hex.png\" width=\"250\"/>\n",
    "</div>\n",
    "   \n",
    "where the cyan spheres are the body of the molecule and the pink spheres are virtual interaction sites that will allow each hexamer to bind to other hexamers. This interaction is attractive and has the following Gaussian functional form:\n",
    "\n",
    "$$ U(r_{ij}) = -A \\exp{(-B r_{ij})} $$\n",
    "\n",
    "where $U$ is the potential energy in kcal/mol, $r_{ij}$ is the pair distance between sites in Angstroms, and $A$/$B$ are model parameters. \n",
    "\n",
    "All of our simulations will be performed using LAMMPS, an open-source MD package developed at Sandia National Lab."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What am I looking at right now?\n",
    "This is a \"Jupyter notebook\", a tool for using Python interactively. For example, you can quickly work with data, run simulations, visualize data, etc. then decide you want to see how your results might change if you change a parameter earlier in your workflow. If you've used Mathematica before, the idea behind a Jupyter notebook is very similar. You hit `shift-enter` or `shift-return` to execute the code in a \"cell\". \n",
    "\n",
    "**Beware:**\n",
    "- the good thing about notebooks is that they let you interact with your data in very flexible ways\n",
    "- the bad thing is that you can execute cells out of order and overwrite variables in ways you forget or didn't expect\n",
    "\n",
    "If you're getting weird results, it's best to either do `Cell->Run All` at the top to reset the entire notebook, or if really needed, `Kernel->Restart`\n",
    "\n",
    "Let us run our first cell (right below) by hitting `shift-enter` inside the cell. This cell will import Python modules that we will need for the rest of the exercise. Note that the left of the cell will have a \"star\" marker (i.e., \"In \\[*\\]\") when it is running and an integer (i.e., \"In \\[1\\]\")when it is done."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "1a330d9f2be244cc87db0f711791941e",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Time to setup the notebook! This might take a few minutes to finish.\n",
    "# We are importing modules/libraries that other folks have created\n",
    "# These libraries contain functions that will use (so that we don't reinvent the wheel)\n",
    "\n",
    "import numpy as np\n",
    "import mdtraj as md\n",
    "import nglview as ngl\n",
    "from lammps import IPyLammps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preparing the LAMMPS simulation ###\n",
    "In your folder, you have the following files: \n",
    "\n",
    "(1) **supercell.pdb** - this is a file format known as the 'Protein Data Bank' format and contains the initial coordinates of our system as well as topology information (names and indices of atoms, residues, etc.)\n",
    "\n",
    "(2) **system.data** - this is an input file used by LAMMPS and contains the coordinates and topology information for a single hexamer, which we will later tile to form the complete system\n",
    "\n",
    "(3) **input.setup** - this is an input file used by LAMMPS that determines most of the simulation parameters (force field definition, integration method, timestep, etc.)\n",
    "\n",
    "All of these files are text files so you should feel free to view their contents using your favorite text editor.\n",
    "\n",
    "In the next cell, we will prepare the LAMMPS simulation using our input files. We can further modify the simulation using the `lammps` module, as you will see."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "LAMMPS output is captured by PyLammps wrapper\n",
      "LAMMPS (23 Jun 2022 - Update 1)\n",
      "OMP_NUM_THREADS environment is not set. Defaulting to 1 thread. (src/comm.cpp:98)\n",
      "  using 1 OpenMP thread(s) per MPI task\n"
     ]
    }
   ],
   "source": [
    "L = IPyLammps() # this prepares the lammps Python object\n",
    "L.variable(\"GAUSSA equal 2.65\") # this sets the \"A\" parameter in the Gaussian to 2.65 kcal/mol\n",
    "L.file(\"input.setup\") # this loads in the other simulation settings\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running the simulation ###\n",
    "\n",
    "Now that the simulation is prepared, we can run the simulation. Let us run a short simulation of 25000 steps to see what happens. You'll see that a file called `md.dcd` will be created.\n",
    "\n",
    "NOTE: This next cell make take a few minutes to run. You will know it is still busy if you see the star in \"In \\[*\\]\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reading data file ...\n",
      "  orthogonal box = (-10 -10 -10) to (10 10 10)\n",
      "  1 by 1 by 1 MPI processor grid\n",
      "  reading atoms ...\n",
      "  27 atoms\n",
      "Finding 1-2 1-3 1-4 neighbors ...\n",
      "  special bond factors lj:    0        0        0       \n",
      "  special bond factors coul:  0        0        0       \n",
      "     0 = max # of 1-2 neighbors\n",
      "     0 = max # of 1-3 neighbors\n",
      "     0 = max # of 1-4 neighbors\n",
      "     1 = max # of special neighbors\n",
      "  special bonds CPU = 0.000 seconds\n",
      "  read_data CPU = 0.018 seconds\n",
      "Replicating atoms ...\n",
      "  orthogonal box = (-10 -10 -10) to (50 50 50)\n",
      "  1 by 1 by 1 MPI processor grid\n",
      "  729 atoms\n",
      "Finding 1-2 1-3 1-4 neighbors ...\n",
      "  special bond factors lj:    0        0        0       \n",
      "  special bond factors coul:  0        0        0       \n",
      "     0 = max # of 1-2 neighbors\n",
      "     0 = max # of 1-3 neighbors\n",
      "     0 = max # of 1-4 neighbors\n",
      "     1 = max # of special neighbors\n",
      "  special bonds CPU = 0.000 seconds\n",
      "  replicate CPU = 0.001 seconds\n",
      "  create bodies CPU = 0.000 seconds\n",
      "  27 rigid bodies with 729 atoms\n",
      "  6.1757105 = max distance from body owner to body atom\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "['Generated 0 of 3 mixed pair_coeff terms from geometric mixing rule',\n",
       " 'Neighbor list info ...',\n",
       " '  update every 1 steps, delay 1 steps, check yes',\n",
       " '  max neighbors/atom: 2000, page size: 100000',\n",
       " '  master list distance cutoff = 10',\n",
       " '  ghost atom cutoff = 10',\n",
       " '  binsize = 5, bins = 12 12 12',\n",
       " '  3 neighbor lists, perpetual/occasional/extra = 3 0 0',\n",
       " '  (1) pair soft, perpetual, skip from (3)',\n",
       " '      attributes: half, newton on',\n",
       " '      pair build: skip',\n",
       " '      stencil: none',\n",
       " '      bin: none',\n",
       " '  (2) pair gauss, perpetual, skip from (3)',\n",
       " '      attributes: half, newton on',\n",
       " '      pair build: skip',\n",
       " '      stencil: none',\n",
       " '      bin: none',\n",
       " '  (3) neighbor class addition, perpetual',\n",
       " '      attributes: half, newton on',\n",
       " '      pair build: half/bin/newton',\n",
       " '      stencil: half/bin/3d',\n",
       " '      bin: standard',\n",
       " 'Setting up Verlet run ...',\n",
       " '  Unit style    : real',\n",
       " '  Current step  : 0',\n",
       " '  Time step     : 10',\n",
       " 'Per MPI rank memory allocation (min/avg/max) = 12.82 | 12.82 | 12.82 Mbytes',\n",
       " '   Step         S/CPU           Temp           f_1           KinEng         PotEng         TotEng         E_pair           Lx             Ly             Lz            Pxx            Pyy            Pzz      ',\n",
       " '         0   0              320.70899      314.76993      50.666547      0              50.666547      0              60             60             60             344.14324      620.18533      514.19345    ',\n",
       " '     25000   21173.593      276.43042      271.31133      43.671288     -25.356243      18.315045     -25.356243      60             60             60            -101.52251     -254.84316     -311.44056    ',\n",
       " 'Loop time of 1.18074 on 1 procs for 25000 steps with 729 atoms',\n",
       " '',\n",
       " 'Performance: 18293.676 ns/day, 0.001 hours/ns, 21173.237 timesteps/s',\n",
       " '99.9% CPU use with 1 MPI tasks x 1 OpenMP threads',\n",
       " '',\n",
       " 'MPI task timing breakdown:',\n",
       " 'Section |  min time  |  avg time  |  max time  |%varavg| %total',\n",
       " '---------------------------------------------------------------',\n",
       " 'Pair    | 0.076191   | 0.076191   | 0.076191   |   0.0 |  6.45',\n",
       " 'Bond    | 0.00068834 | 0.00068834 | 0.00068834 |   0.0 |  0.06',\n",
       " 'Neigh   | 0.38735    | 0.38735    | 0.38735    |   0.0 | 32.81',\n",
       " 'Comm    | 0.082391   | 0.082391   | 0.082391   |   0.0 |  6.98',\n",
       " 'Output  | 0.00073046 | 0.00073046 | 0.00073046 |   0.0 |  0.06',\n",
       " 'Modify  | 0.58151    | 0.58151    | 0.58151    |   0.0 | 49.25',\n",
       " 'Other   |            | 0.05187    |            |       |  4.39',\n",
       " '',\n",
       " 'Nlocal:            729 ave         729 max         729 min',\n",
       " 'Histogram: 1 0 0 0 0 0 0 0 0 0',\n",
       " 'Nghost:            858 ave         858 max         858 min',\n",
       " 'Histogram: 1 0 0 0 0 0 0 0 0 0',\n",
       " 'Neighs:              0 ave           0 max           0 min',\n",
       " 'Histogram: 1 0 0 0 0 0 0 0 0 0',\n",
       " '',\n",
       " 'Total # of neighbors = 0',\n",
       " 'Ave neighs/atom = 0',\n",
       " 'Ave special neighs/atom = 0',\n",
       " 'Neighbor list builds = 1577',\n",
       " 'Dangerous builds = 0']"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "L.run(25000) #run for 25000 steps, should take a few seconds to run"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualizing the trajectory ###\n",
    "\n",
    "You'll see in the output of the previous block that a bunch of text was printed out. This is the \"log\" of the simulation and contains useful information. For our purposes, we can skip this and instead visualize the actual trajectory.\n",
    "\n",
    "We will use `nglview` to display the trajectory. Once it loads, you can click play to see the atoms in motion!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "6028db0fe09842f0a5d40dca7403741b",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "NGLWidget(max_frame=25)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "traj = md.load(\"md.dcd\", top=\"supercell.pdb\")\n",
    "view = ngl.show_mdtraj(traj)\n",
    "view.clear_representations()\n",
    "\n",
    "# show all sites as either cyan or pink\n",
    "view.add_spacefill(selection=\".1 or .2\", color=\"cyan\", radius=3.0)\n",
    "view.add_spacefill(selection=\".3\", color=\"pink\", radius=3.0)\n",
    "\n",
    "# add to output\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extending the simulation ###\n",
    "\n",
    "Let us extend the simulations now. Using the current parameters, we should expect to see self-assembly after about 1,000,000 (1M or 1e6) steps. \n",
    "\n",
    "NOTE: This next cell make take a few minutes to run. You will know it is still busy if you see the star in \"In \\[*\\]\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Generated 0 of 3 mixed pair_coeff terms from geometric mixing rule',\n",
       " 'Setting up Verlet run ...',\n",
       " '  Unit style    : real',\n",
       " '  Current step  : 25000',\n",
       " '  Time step     : 10',\n",
       " 'Per MPI rank memory allocation (min/avg/max) = 12.82 | 12.82 | 12.82 Mbytes',\n",
       " '   Step         S/CPU           Temp           f_1           KinEng         PotEng         TotEng         E_pair           Lx             Ly             Lz            Pxx            Pyy            Pzz      ',\n",
       " '     25000   0              276.43042      271.31133      43.671288     -25.356243      18.315045     -25.356243      60             60             60             9.0449327      9.2706221      9.4062472    ',\n",
       " '     50000   17270.473      279.54641      274.36962      44.163561     -32.762679      11.400882     -32.762679      60             60             60            -372.61076     -22.837494      349.64033    ',\n",
       " '     75000   19563.319      289.56266      284.20039      45.745958     -8.98332        36.762638     -8.98332        60             60             60            -145.93269      155.88335      274.69909    ',\n",
       " '    100000   20759.829      338.22356      331.96016      53.433549     -29.577164      23.856385     -29.577164      60             60             60            -312.8851      -103.84198     -133.73196    ',\n",
       " '    125000   19654.592      398.79739      391.41225      63.003181     -32.212122      30.791059     -32.212122      60             60             60            -301.16865     -564.14948      209.16764    ',\n",
       " '    150000   17826.064      251.75874      247.09654      39.773584     -68.485562     -28.711978     -68.485562      60             60             60             198.97984      136.25085     -155.81128    ',\n",
       " '    175000   17289.453      305.22096      299.56872      48.219703     -84.909988     -36.690285     -84.909988      60             60             60            -687.21382     -431.159       -196.02035    ',\n",
       " '    200000   16709.116      255.13033      250.4057       40.306238     -107.9804      -67.674159     -107.9804       60             60             60            -255.9344      -399.95908     -64.177437    ',\n",
       " '    225000   14223.226      338.65815      332.3867       53.502207     -134.37731     -80.875099     -134.37731      60             60             60             378.78293      11.407272      191.3056     ',\n",
       " '    250000   12868.792      316.21052      310.35477      49.955865     -180.57628     -130.62041     -180.57628      60             60             60             225.02515     -145.2677      -276.35095    ',\n",
       " '    275000   12120.906      384.94042      377.8119       60.814017     -154.18229     -93.368273     -154.18229      60             60             60             305.49134      133.77207     -2.1584335    ',\n",
       " '    300000   12327.746      282.83766      277.59992      44.683522     -186.43477     -141.75125     -186.43477      60             60             60            -185.01015     -42.428295      316.05504    ',\n",
       " '    325000   11864.354      285.17605      279.89501      45.052949     -194.50011     -149.44716     -194.50011      60             60             60             281.02657      2.5210249      46.550211    ',\n",
       " '    350000   11362.133      312.24934      306.46694      49.330066     -191.19781     -141.86774     -191.19781      60             60             60             492.73796      99.726658      86.584617    ',\n",
       " '    375000   11490.555      281.98783      276.76584      44.549265     -166.45933     -121.91006     -166.45933      60             60             60             477.03949      254.78037      74.615779    ',\n",
       " '    400000   11429.833      253.45613      248.76249      40.041742     -178.70288     -138.66113     -178.70288      60             60             60             288.66888     -40.177591      545.46495    ',\n",
       " '    425000   10848.066      321.47051      315.51735      50.786854     -197.06569     -146.27884     -197.06569      60             60             60            -65.130905     -26.959892     -44.666854    ',\n",
       " '    450000   10988.537      267.59978      262.64422      42.276197     -176.68268     -134.40649     -176.68268      60             60             60             171.44174     -197.02804     -339.6747     ',\n",
       " '    475000   11474.596      305.01537      299.36694      48.187223     -201.13827     -152.95105     -201.13827      60             60             60            -311.40615      174.13493      297.16855    ',\n",
       " '    500000   11187.339      307.25081      301.56098      48.540383     -206.49126     -157.95088     -206.49126      60             60             60             98.85011      -114.41358      160.28833    ',\n",
       " '    525000   10685.184      273.35596      268.29381      43.185576     -215.70199     -172.51641     -215.70199      60             60             60            -26.844993      135.87638     -267.38992    ',\n",
       " '    550000   10873.852      314.93271      309.10062      49.753993     -240.33737     -190.58338     -240.33737      60             60             60             272.6319      -50.869999      185.30898    ',\n",
       " '    575000   10610.265      357.18922      350.5746       56.4298       -241.11035     -184.68055     -241.11035      60             60             60            -109.14099      77.822216      130.18062    ',\n",
       " '    600000   10268.032      316.09879      310.24511      49.938214     -232.74819     -182.80998     -232.74819      60             60             60            -236.23766      172.53431     -35.858125    ',\n",
       " '    625000   10614.005      259.31479      254.51266      40.96731      -229.16591     -188.1986      -229.16591      60             60             60             298.83722     -223.30682     -86.759145    ',\n",
       " '    650000   10025.538      268.82728      263.849        42.470123     -245.61177     -203.14165     -245.61177      60             60             60            -457.38561      90.905136      358.54092    ',\n",
       " '    675000   9860.6222      339.95798      333.66246      53.707559     -227.27251     -173.56495     -227.27251      60             60             60             63.391009      122.81707     -4.3108823    ',\n",
       " '    700000   9807.3461      297.5214       292.01175      47.003304     -232.82232     -185.81901     -232.82232      60             60             60             281.25481     -169.88265      454.8326     ',\n",
       " '    725000   9214.5985      312.83643      307.04316      49.422817     -257.46356     -208.04074     -257.46356      60             60             60             435.8463      -4.1315356      27.00533     ',\n",
       " '    750000   7154.283       250.71468      246.07182      39.608641     -266.36372     -226.75508     -266.36372      60             60             60             89.382032      346.80599     -131.34818    ',\n",
       " '    775000   9702.2513      265.2202       260.30872      41.900266     -253.99541     -212.09514     -253.99541      60             60             60            -18.86121      -17.601983      58.094111    ',\n",
       " '    800000   10246.949      342.69525      336.34904      54.140001     -257.6641      -203.5241      -257.6641       60             60             60            -614.86871     -4.1140247      68.718632    ',\n",
       " '    825000   9807.7185      347.67056      341.23221      54.926014     -254.34279     -199.41678     -254.34279      60             60             60            -315.33928      36.583088      239.95757    ',\n",
       " '    850000   10191.013      315.94125      310.09048      49.913325     -258.14388     -208.23056     -258.14388      60             60             60             373.59835     -62.369914      58.74839     ',\n",
       " '    875000   9882.6643      332.15832      326.00724      52.475345     -254.87279     -202.39744     -254.87279      60             60             60             383.71159     -100.22671      352.82054    ',\n",
       " '    900000   10065.42       284.94677      279.66998      45.016726     -261.89336     -216.87664     -261.89336      60             60             60            -606.36008      16.3322       -45.899399    ',\n",
       " '    925000   10858.683      285.23163      279.94956      45.06173      -254.70345     -209.64172     -254.70345      60             60             60             80.850747     -68.001383      26.490229    ',\n",
       " '    950000   11137.181      272.84932      267.79655      43.105536     -259.9675      -216.86197     -259.9675       60             60             60            -617.84694     -190.93089     -39.603731    ',\n",
       " '    975000   10574.505      289.34442      283.98619      45.71148      -269.93607     -224.22459     -269.93607      60             60             60            -4.0276043      137.54226      5.8143375    ',\n",
       " '   1000000   10866.675      284.48282      279.21462      44.94343      -269.08136     -224.13793     -269.08136      60             60             60            -376.07777      14.697         13.176854    ',\n",
       " '   1025000   10518.672      283.78062      278.52542      44.832494     -236.91646     -192.08396     -236.91646      60             60             60             8.3111583      43.252681     -97.692191    ',\n",
       " 'Loop time of 87.1919 on 1 procs for 1000000 steps with 729 atoms',\n",
       " '',\n",
       " 'Performance: 9909.176 ns/day, 0.002 hours/ns, 11468.954 timesteps/s',\n",
       " '100.2% CPU use with 1 MPI tasks x 1 OpenMP threads',\n",
       " '',\n",
       " 'MPI task timing breakdown:',\n",
       " 'Section |  min time  |  avg time  |  max time  |%varavg| %total',\n",
       " '---------------------------------------------------------------',\n",
       " 'Pair    | 13.888     | 13.888     | 13.888     |   0.0 | 15.93',\n",
       " 'Bond    | 0.027906   | 0.027906   | 0.027906   |   0.0 |  0.03',\n",
       " 'Neigh   | 38.334     | 38.334     | 38.334     |   0.0 | 43.96',\n",
       " 'Comm    | 5.0466     | 5.0466     | 5.0466     |   0.0 |  5.79',\n",
       " 'Output  | 1.0335     | 1.0335     | 1.0335     |   0.0 |  1.19',\n",
       " 'Modify  | 26.408     | 26.408     | 26.408     |   0.0 | 30.29',\n",
       " 'Other   |            | 2.454      |            |       |  2.81',\n",
       " '',\n",
       " 'Nlocal:            729 ave         729 max         729 min',\n",
       " 'Histogram: 1 0 0 0 0 0 0 0 0 0',\n",
       " 'Nghost:            983 ave         983 max         983 min',\n",
       " 'Histogram: 1 0 0 0 0 0 0 0 0 0',\n",
       " 'Neighs:              0 ave           0 max           0 min',\n",
       " 'Histogram: 1 0 0 0 0 0 0 0 0 0',\n",
       " '',\n",
       " 'Total # of neighbors = 0',\n",
       " 'Ave neighs/atom = 0',\n",
       " 'Ave special neighs/atom = 0',\n",
       " 'Neighbor list builds = 60858',\n",
       " 'Dangerous builds = 0']"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "L.run(1000000) #run for 1M steps, should take a few minutes to run (80 sec on my machine)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traj = md.load(\"md.dcd\", top=\"supercell.pdb\")\n",
    "view = ngl.show_mdtraj(traj)\n",
    "view.clear_representations()\n",
    "\n",
    "# show all sites as either cyan or pink\n",
    "view.add_spacefill(selection=\".1 or .2\", color=\"cyan\", radius=3.0)\n",
    "view.add_spacefill(selection=\".3\", color=\"pink\", radius=3.0)\n",
    "\n",
    "# add to output\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Observation debrief ###\n",
    "\n",
    "What did you observe when you played those movies? Did you see aggregation of the hexamers into a cluster? What shape is the cluster?\n",
    "\n",
    "Keep this behavior in mind because now we are going to adjust the model parameters to see what happens. You should change the $A$ parameter to be lower (e.g., 2.0 kcal/mol) or higher (e.g., 4.0 kcal/mol). To do so, go back to the top of the notebook and change the line that originally says:\n",
    "\n",
    "`L.variable(\"GAUSSA equal 2.65\")`\n",
    "\n",
    "Note that the original simulation uses 2.65 kcal/mol. What do you think will happen in either scenario? Do you notice any changes in assembly behavior? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
