{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating a codon table"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we are more familiar with the basics of Python 3, we are going to move on to more advanced functions. In particular, we are going to cover an important concept known as \"functions\". We will use \"functions\" to create a codon table, which will then let us convert any DNA/RNA nucleotide sequence into a protein sequence."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we have blocks of code that we want to use over and over again, we can wrap them inside a \"function\" block that can be \"called\" (i.e., used) later on in the code. Usually, programmers think about how a process can be generalized where the specific details of the inputs may vary but the process itself is the same.\n",
    "\n",
    "For example, think about a recipe to cook a meat pie. The inputs may vary (meat = lamb vs. meat = beef) but the steps you take to make the meat pie will be the same (at least, for simple recipes).  \n",
    "\n",
    "Below, we have an example of a function that takes in any two numbers in variables x and y, then prints their sum. Notice again the use of indents to define the code block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# the variables x and y are supplied by the user\n",
    "def add_then_print(x, y) :\n",
    "    z = x + y # the function creates a new variable z that is the sum of x and y\n",
    "    print(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the function is defined, we can call the function later on in the code, which will execute the function. We can call the function as many times as we'd like. We can also change the input variables as many times as we'd like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "17\n",
      "10\n",
      "11\n"
     ]
    }
   ],
   "source": [
    "xx = 9\n",
    "yy = 2\n",
    "add_then_print(10, 7)\n",
    "add_then_print(5, 5)\n",
    "add_then_print(xx, yy) # notice that the variables don't need to be the same as those in the function definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the above example, the function immediately prints out the value of x + y.\n",
    "However, instead of printing, we can have the function evaluate itself so that the user can customize what to do with the result. \n",
    "We accomplish this using a \"return\" statement. Note that the \"return\" statement always indicates the end of the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "17\n",
      "10\n",
      "The total sum is  27\n"
     ]
    }
   ],
   "source": [
    "def add_then_return(x, y) :\n",
    "    z = x + y\n",
    "    return z\n",
    "\n",
    "z1 = add_then_return(10, 7)\n",
    "print(z1)\n",
    "z2 = add_then_return(5, 5)\n",
    "print(z2)\n",
    "z3 = add_then_return(z1, z2)\n",
    "print('The total sum is ', z3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Codon Tables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "During DNA/RNA to protein translation, specialized machinery is able to interpret DNA/RNA sequences and convert them into a string of amino acids, which will eventually form into proteins. \n",
    "\n",
    "DNA/RNA is composed of four types of nucleotides:\n",
    "\n",
    "**A** = adenine\n",
    "\n",
    "**C** = cytosine\n",
    "\n",
    "**G** = guanine\n",
    "\n",
    "**T/U** = thymine/uracil (T is in DNA, while U is in RNA)\n",
    "\n",
    "So if a RNA sequence looks like \"AUGUAA\", the nucleotides are adenine, uracil, guanine, uracil, adenine, and adenine."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ribosomes read DNA/RNA sequences in triplets (i.e., codons) to convert to one of the twenty natural amino acids. This conversion follows the convention defined in the following wheel diagram (using RNA sequences):\n",
    "\n",
    "![Codon Wheel](img/codon_wheel.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All proteins begin translation with Met (M), which uses the \"start codon\" AUG. Each triplet of RNA sequences will create a new amino acid until the \"stop codon\" (i.e., UAA, UAG, or UGA) is read.\n",
    "\n",
    "For example, RNA sequence \"AUGGCAGCUUAA\" will create the amino acid sequence \"MAA\" since:\n",
    "\n",
    "AUG - Met (M)\n",
    "\n",
    "GCA - Ala (A)\n",
    "\n",
    "GCU - Ala (A)\n",
    "\n",
    "UAA - Stop\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of looking at the wheel diagram every time we need to convert RNA sequences to protein sequences, we would like to create a series of functions to automate the process."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convert codon to amino acid one-letter-code "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us first write a function that converts any string of three RNA nucleotides to its corresponding amino acid. We will use the fact that strings can be represented as a list of characters and Boolean logic to complete this task.\n",
    "\n",
    "I have provided an incomplete version of the function. Right now, it only detects start and stop codons. There are multiple ways to code the logic for this codon table, but all of them will require Boolean logic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "invalid syntax (198820180.py, line 21)",
     "output_type": "error",
     "traceback": [
      "\u001b[0;36m  Input \u001b[0;32mIn [5]\u001b[0;36m\u001b[0m\n\u001b[0;31m    else :\u001b[0m\n\u001b[0m    ^\u001b[0m\n\u001b[0;31mSyntaxError\u001b[0m\u001b[0;31m:\u001b[0m invalid syntax\n"
     ]
    }
   ],
   "source": [
    "### PLEASE FINISH THIS FUNCTION\n",
    "### IT IS CURRENTLY INCOMPLETE\n",
    "\n",
    "def convert_codon_to_aa(codon_str) :\n",
    "    '''\n",
    "    Inputs:\n",
    "    codon_str = A string of 3 letters where each letter is either A, C, G, or U\n",
    "    Return:\n",
    "    aa_code = A string of 1 letter that represents the amino acid; if stop codon, return \"<end>\" \n",
    "    '''\n",
    "    \n",
    "    # check if it is the start codon\n",
    "    if codon_str[0] == 'A' and codon_str[1] == 'U' and codon_str[2] == 'G' :\n",
    "        return \"M\"\n",
    "    # the next two check if it is the end codon\n",
    "    elif codon_str[0] == 'U' and codon_str[1] == 'A' and (codon_str[2] == 'G' or codon_str[2] == 'A'):\n",
    "        return \"<end>\"\n",
    "    elif codon_str[0] == 'U' and codon_str[1] == 'G' and codon_str[2] == 'A':\n",
    "        return \"<end>\"\n",
    "    # below this comment, you should include more \"elif\" statements to find the other codons\n",
    "    # this might be long (~20-50 lines)\n",
    "    ?????\n",
    "    ?????\n",
    "    # you do not have to modify below this comment\n",
    "    else :\n",
    "        return \"Amino acid not found\"\n",
    "\n",
    "# let us check the output of the function we just created\n",
    "# if the result is \"None\", then the function does not have the correct logic for your input codon\n",
    "print(convert_codon_to_aa(\"UGA\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have a complete function, we can write another function to take an entire RNA sequence and convert it into an amino acid sequence. I have written this function for you below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'convert_codon_to_aa' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Input \u001b[0;32mIn [6]\u001b[0m, in \u001b[0;36m<cell line: 26>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     24\u001b[0m \u001b[38;5;66;03m# test function - this should return MMM\u001b[39;00m\n\u001b[1;32m     25\u001b[0m test_seq \u001b[38;5;241m=\u001b[39m \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124mUGAAUGAUGAUGUAAAUG\u001b[39m\u001b[38;5;124m\"\u001b[39m\n\u001b[0;32m---> 26\u001b[0m \u001b[38;5;28mprint\u001b[39m( \u001b[43mconvert_RNA_to_protein\u001b[49m\u001b[43m(\u001b[49m\u001b[43mtest_seq\u001b[49m\u001b[43m)\u001b[49m )\n\u001b[1;32m     28\u001b[0m \u001b[38;5;66;03m# test function - this should return MAQKT\u001b[39;00m\n\u001b[1;32m     29\u001b[0m test_seq \u001b[38;5;241m=\u001b[39m \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124mAUGGCACAGAAAACAUAG\u001b[39m\u001b[38;5;124m\"\u001b[39m\n",
      "Input \u001b[0;32mIn [6]\u001b[0m, in \u001b[0;36mconvert_RNA_to_protein\u001b[0;34m(rna_seq)\u001b[0m\n\u001b[1;32m     11\u001b[0m \u001b[38;5;28;01mfor\u001b[39;00m i \u001b[38;5;129;01min\u001b[39;00m \u001b[38;5;28mrange\u001b[39m(\u001b[38;5;28mint\u001b[39m(len_seq\u001b[38;5;241m/\u001b[39m\u001b[38;5;241m3\u001b[39m)) :\n\u001b[1;32m     12\u001b[0m     codon \u001b[38;5;241m=\u001b[39m rna_seq[(\u001b[38;5;241m3\u001b[39m\u001b[38;5;241m*\u001b[39mi):(\u001b[38;5;241m3\u001b[39m\u001b[38;5;241m*\u001b[39m(i\u001b[38;5;241m+\u001b[39m\u001b[38;5;241m1\u001b[39m))] \u001b[38;5;66;03m# a shortcut to get the next set of 3 letters\u001b[39;00m\n\u001b[0;32m---> 13\u001b[0m     aa_code \u001b[38;5;241m=\u001b[39m \u001b[43mconvert_codon_to_aa\u001b[49m(codon)\n\u001b[1;32m     14\u001b[0m     \u001b[38;5;28;01mif\u001b[39;00m(aa_code \u001b[38;5;241m==\u001b[39m \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124mM\u001b[39m\u001b[38;5;124m\"\u001b[39m) :\n\u001b[1;32m     15\u001b[0m         start_flag \u001b[38;5;241m=\u001b[39m \u001b[38;5;241m1\u001b[39m\n",
      "\u001b[0;31mNameError\u001b[0m: name 'convert_codon_to_aa' is not defined"
     ]
    }
   ],
   "source": [
    "### THIS NEXT FUNCTION IS ALREADY COMPLETE\n",
    "### PLEASE DO NOT CHANGE IT\n",
    "def convert_RNA_to_protein(rna_seq) :\n",
    "    # first, check that the rna_seq has the right size\n",
    "    len_seq = len(rna_seq)\n",
    "    if (len_seq%3 != 0) :\n",
    "        return \"ERROR: RNA seq is not the right length\"\n",
    "    \n",
    "    start_flag = 0 # turn this on when MET is found\n",
    "    prot_seq = \"\" # placeholder for protein sequence\n",
    "    for i in range(int(len_seq/3)) :\n",
    "        codon = rna_seq[(3*i):(3*(i+1))] # a shortcut to get the next set of 3 letters\n",
    "        aa_code = convert_codon_to_aa(codon)\n",
    "        if(aa_code == \"M\") :\n",
    "            start_flag = 1\n",
    "        elif(aa_code == \"<end>\") :\n",
    "            start_flag = 0\n",
    "        # add letter to sequence if start_flag=1\n",
    "        if(start_flag) :\n",
    "            prot_seq += aa_code\n",
    "\n",
    "    return prot_seq\n",
    "\n",
    "# test function - this should return MMM\n",
    "test_seq = \"UGAAUGAUGAUGUAAAUG\"\n",
    "print( convert_RNA_to_protein(test_seq) )\n",
    "\n",
    "# test function - this should return MAQKT\n",
    "test_seq = \"AUGGCACAGAAAACAUAG\"\n",
    "print( convert_RNA_to_protein(test_seq) )\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now check your code with the following RNA sequences:\n",
    "\n",
    "1) AUGAUAAAUGAGAGUUAA\n",
    "\n",
    "2) AUGCAUGAGCUGUUACAGUGGCAAAGGCUCGAUUGAGAGUGG\n",
    "\n",
    "3) AAAAAAAUGUGUCAAGACGAAGACGAAAAUGUCGAGCGGUAG\n",
    "\n",
    "In the code block below, define the appropriate variables and function calls for each example. What amino acid sequence did you get?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Object `????` not found.\n"
     ]
    }
   ],
   "source": [
    "??????"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Don't change below this section\n",
    "\n",
    "You will need this protein sequence for the next activity (copy/paste it when told):\n",
    "\n",
    "VNSDVLTVSTVNSQDQVTQKPLRDSVKQALKYFAQLNGQ\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
